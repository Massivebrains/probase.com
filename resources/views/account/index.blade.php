@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 m-b-30">
        <div class="d-block d-sm-flex flex-nowrap align-items-center">
            <div class="page-title mb-2 mb-sm-0">
                <h1>Banking Details</h1>
            </div>
        </div>
    </div>

</div>

<div class="row tabs-contant">
    <div class="col-xxl-12  ">
        <div class="card card-statistics">
            <div class="card-body">

                <form action="/account" class="col-sm-12 col-md-12 col-lg-12" method="POST">

                        @csrf

                        <div class="row">

                            <div class="form-group col-sm-12 col-md-12 col-lg-6">
                                <label for="referenceNumber">Bank Name</label>
                                <input type="text" value="{{optional($account)->bank_name}}" name="bank_name" required class="form-control  input-box b-white">
                            </div>
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="accountName">Account Name</label>
                                <input type="text" value="{{optional($account)->account_name}}" name="account_name" required class="form-control  input-box b-white">
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="transactionDate">Bank Account Number</label>
                                <input type="text" name="account_number" value="{{optional($account)->account_number}}" required class="form-control  input-box b-white">
                            </div>
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="paymentDate">Bank Sort Code</label>
                                <input type="text" name="sort_code" value="{{optional($account)->sort_code}}" required class="form-control  input-box b-white">
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group col-12">
                                <button type="submit" class="btn btn-primary">Update Account Details</button>
                            </div>
                        </div>

                    </form>

            </div>
        </div>
    </div>
</div>

@endsection