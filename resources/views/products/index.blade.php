@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 m-b-30">
        <div class="d-block d-sm-flex flex-nowrap align-items-center">
            <div class="page-title mb-2 mb-sm-0">
                <h1>Products</h1>
            </div>
        </div>
    </div>

    <div class="col-md-4 m-b-30">
        <a href="/product/0/event" class="float-right btn btn-primary  ml-2">Add New Event</a>
        <a href="/product/0/product" class="float-right btn btn-primary">Add New Product</a>
    </div>

</div>

<div class="row tabs-contant">
    <div class="col-xxl-12  ">
        <div class="card card-statistics">
            <div class="card-body">
                <div class="tab round">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" id="home-07-tab" data-toggle="tab" href="#home-07" role="tab" aria-controls="home-07" aria-selected="true">
                                <i class="fa fa-shopping-cart"></i>
                                Products
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-07-tab" data-toggle="tab" href="#profile-07" role="tab" aria-controls="profile-07" aria-selected="false">
                                <i class="fa fa-calendar"></i> Events
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane fade py-3 active show" id="home-07" role="tabpanel" aria-labelledby="home-07-tab">

                            <table id="services" class="table table-striped w-100">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($products as $row)
                                    <tr>
                                        <td>{{$row->product_code}}</td>
                                        <td>{{$row->product_name}}</td>
                                        <td>{{_c($row->product_price)}}</td>
                                        <td>{{_badge($row->status)}}</td>
                                        <td><a href="/product/{{$row->id}}">View</a></td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>                            
                        </div>

                        
                        <div class="tab-pane fade py-3" id="profile-07" role="tabpanel" aria-labelledby="profile-07-tab">

                            <table id="events" class="table table-striped w-100">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($events as $row)
                                    <tr>
                                        <td>{{$row->product_code}}</td>
                                        <td>{{$row->product_name}}</td>
                                        <td>{{_c($row->product_price)}}</td>
                                        <td>{{_badge($row->status)}}</td>
                                        <td><a href="/product/{{$row->id}}">View</a></td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection