<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Ogaranya;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login', ['bg' => 'bg-purple']);
    }

    public function login(Request $request)
    {
        $this->validate($request, [

            'email'		=> 'required|email',
            'password'	=> 'required'
        ]);

        $payload = [

            'email'         => request('email'),
            'password'      => request('password'),
            'premium_id'    => 170
        ];

        $response = Ogaranya::post('auth/login', $payload);

        if ($response->status == 'Failed') {
            return redirect()->back()->with('error', 'Invalid Username or Password');
        }

        session()->put('api_token', $response->data->api_token);
        session()->put('user', $response->data);

        return redirect('/');
    }

    public function logout()
    {
        session()->pull('api_token');
        session()->pull('user');

        return redirect('/login')->with('message', 'You have been successfully logged out.');
    }

    public function forgotPassword()
    {
        if (request()->isMethod('post')) {
            $payload = [

                'email'         => request('email'),
                'premium_id'    => 170
            ];

            $response = Ogaranya::post('auth/find-merchant', $payload);

            if ($response->status == 'Failed') {
                return redirect()->back()->with('error', 'We do not recognize this email address.');
            }

            $data = [

                'url'       => url('/reset-password/'.md5(request('email'))),
                'merchant'  => $response->data
            ];

            session()->put('email', request('email'));

            $body  = view('emails.reset-password', $data)->render();

            _email(request('email'), 'Reset your password on getionpay.com', $body);

            return redirect()->back()->with('message', 'A reset password email has been sent to '.request('email').' Click on the link in the email to reset your password.');
        }

        $data['title']  = 'Forgot Password';

        return view('auth.forgot-password', $data);
    }

    public function resetPassword(Request $request, $token = null)
    {
        if (request()->isMethod('post')) {
            $payload = [

                'email'                 => session('email'),
                'password'               => request('password'),
                'password_confirmation' => request('password_confirmation'),
                'premium_id'            => 170
            ];

            $response = Ogaranya::post('auth/change-password', $payload);

            if ($response->status == 'Failed') {
                return redirect()->back()->with('error', $response->data);
            }

            session()->pull('email');

            return redirect('/login')->with('message', 'Password has been successfully changed. you can now login.');
        } else {
            if (md5(session('email')) != $token) {
                return redirect('/login')->with('error', 'Reset Password link has expired.');
            }
        }

        $data['title']  = 'Reset Password';

        return view('auth.reset-password', $data);
    }
}
