@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-sm-12 col-lg-12">

		<form action="/product/{{$product->id}}" method="POST">

			@csrf
			<div class="row">

				<div class="form-group col-sm-12 col-lg-12">
					<label for="accountName">{{ucfirst($type)}} Name</label>
					<input type="text" value="{{$product->product_name}}" name="product_name" class="form-control input-box b-white" required>
				</div>

			</div>

			<div class="row">

				<div class="form-group col-sm-6 col-lg-6">
					<label for="transactionDate">{{ucfirst($type)}} Quantity</label>
					<input type="number" name="quantity" value="{{$product->quantity}}" class="form-control input-box b-white" required>
				</div>

				<div class="form-group col-sm-6 col-lg-6">
					<label for="transactionDate">{{ucfirst($type)}} Price</label>
					<input type="number" name="product_price" value="{{$product->product_price}}" class="form-control input-box b-white" required>
				</div>                                

			</div>

			<div class="row">

				<div class="form-group col-sm-12 col-lg-12">
					<label for="paymentDate">{{ucfirst($type)}} Description</label>
					<textarea name="desc" class="form-control" rows="3" required>{{$product->desc}}</textarea>
				</div>

			</div>

			<div class="row">
				<div class="form-group col-12">
					<input type="hidden" name="product_type" value="{{$type}}">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>
			</div>

		</form>

	</div>
</div>

@endsection