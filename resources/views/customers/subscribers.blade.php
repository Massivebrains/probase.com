@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 m-b-30">
        <div class="d-block d-sm-flex flex-nowrap align-items-center">
            <div class="page-title mb-2 mb-sm-0">
                <h1>Orders</h1>
            </div>
        </div>
    </div>

</div>

<div class="row tabs-contant">
    <div class="col-xxl-12  ">
        <div class="card card-statistics">
            <div class="card-body">

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Phone Number</th>
                            <th>Info</th>
                            <th>Command</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subscribers as $row)
                        <tr>
                            <td>{{$row->msisdn}}</td>
                            <td>{{$row->log}}</td>
                            <td>{{$row->command->custom_command}}</td>
                            <td>{{$row->created_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection