<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="/img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/vendors.css" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/cards.css" rel="stylesheet" type="text/css" />

</head>

<body class="light-sidebar">

    <div class="app">

        <div class="app-wrap">

            <div class="loader">
                <div class="h-100 d-flex justify-content-center">
                    <div class="align-self-center">
                        <img src="/img/loader/loader.svg" alt="loader">
                    </div>
                </div>
            </div>

            @include('includes.header')


            <div class="app-container">

                @include('includes.sidebar')

                <div class="app-main" id="main">

                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-12 mb-2">
                                @include('includes.alert')
                            </div>
                        </div>

                        @yield('content')

                    </div>

                </div>

            </div>

            <footer class="footer" style="background-color: #f2f2f2;">
                <div class="row">
                    <div class="col-12 col-sm-12 text-center text-sm-left">
                        <p>&copy; Copyright {{ date('Y') }}. All rights reserved.</p>
                    </div>
                </div>
            </footer>

        </div>

    </div>

    <script src="/js/vendors.js"></script>
    <script src="/js/app.js"></script>
</body>

</html>
