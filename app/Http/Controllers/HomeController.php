<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Ogaranya;

class HomeController extends Controller
{
    public function index()
    {
    	$data['active']		= 'dashboard';
    	
    	return view('home.index', $data);
    }
}
