<aside class="app-navbar">

    <div class="sidebar-nav scrollbar scroll_dark">
        <ul class="metismenu " id="sidebarNav">
            <li class="nav-static-title">System Menu</li>

            <li>
                <a href="/" aria-expanded="false">
                    <i class="nav-icon ti ti-home"></i>
                    <span class="nav-title">Dashboard</span>
                </a>
            </li>

            <li>
                <a href="/merchants" aria-expanded="false">
                    <i class="nav-icon ti ti-user"></i>
                    <span class="nav-title">Merchants</span>
                </a>
            </li>

            <li>
                <a href="/products" aria-expanded="false">
                    <i class="nav-icon ti ti-shopping-cart"></i>
                    <span class="nav-title">Services</span>
                </a>
            </li>

            <li>
                <a href="/orders" aria-expanded="false">
                    <i class="nav-icon ti ti-credit-card"></i>
                    <span class="nav-title">Orders</span>
                </a>
            </li>

            <li>
                <a href="/wallet" aria-expanded="false">
                    <i class="nav-icon ti ti-wallet"></i>
                    <span class="nav-title">Wallet</span>
                </a>
            </li>

            <li>
                <a href="/customers" aria-expanded="false">
                    <i class="nav-icon ti ti-user"></i>
                    <span class="nav-title">Customers</span>
                </a>
            </li>

            <li>
                <a href="/subscribers" aria-expanded="false">
                    <i class="nav-icon ti ti-world"></i>
                    <span class="nav-title">Subscribers</span>
                </a>
            </li>



            <li class="nav-static-title">Account</li>

            <li>
                <a href="/account" aria-expanded="false">
                    <i class="nav-icon ti ti-layout-tab-window"></i>
                    <span class="nav-title">Banking Details</span>
                </a>
            </li>

            <li>
                <a href="/settings" aria-expanded="false">
                    <i class="nav-icon ti ti-settings"></i>
                    <span class="nav-title">Profile</span>
                </a>
            </li>

            <li>
                <a href="mailto:vadeshayo@gmail.com" aria-expanded="false">
                    <i class="nav-icon ti ti-info"></i>
                    <span class="nav-title">Help</span>
                </a>
            </li>

            <li>
                <a href="/logout" aria-expanded="false">
                    <i class="zmdi zmdi-power"></i>
                    <span class="nav-title">Log out</span>
                </a>
            </li>

        </ul>
    </div>

</aside>
