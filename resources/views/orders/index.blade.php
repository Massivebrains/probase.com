@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 m-b-30">
        <div class="d-block d-sm-flex flex-nowrap align-items-center">
            <div class="page-title mb-2 mb-sm-0">
                <h1>Orders</h1>
            </div>
        </div>
    </div>

</div>

<div class="row tabs-contant">
    <div class="col-xxl-12  ">
        <div class="card card-statistics">
            <div class="card-body">

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Reference</th>
                            <th>Customer</th>
                            <th>Order Date</th>
                            <th>Payment Date</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $row)
                        <tr>
                            <td>{{$row->order_reference}}</td>
                            <td>{{$row->msisdn}}</td>
                            <td>{{_d($row->date_ordered)}}</td>
                            <td>{{_d($row->payment_date)}}</td>
                            <td>{{_c($row->total)}}</td>
                            <td>{{_badge($row->order_status)}}</td>
                            <td><a href="/order/{{$row->order_id}}" class="btn btn-outline-blue btn-sm">View Details</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@endsection