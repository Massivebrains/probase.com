<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Ogaranya;

class AccountController extends Controller
{
    public function index()
    {
        $data['active'] 	= 'account';
        $data['account']    = Ogaranya::get('account-detail')->data;

        return view('account.index', $data);
    }

    public function save(Request $request, $id = 0)
    {
        $payload = collect(request()->all())->except('_token')->all();

        $response = Ogaranya::post('account-detail', $payload);

        if ($response->status == 'Successful') {
            return redirect('/account')->with('message', 'Account Details Updated Successfully.');
        }

        return redirect()->back()->with('error', (string)$response->data);
    }

    public function settings()
    {
        $data['active'] = 'settings';
        $data['user']	= session('user');

        return view('account.settings', $data);
    }

    public function password(Request $request, $id = 0)
    {
        if (session('user')->merchant_id == 170) {
            return redirect('/settings')->with('error', 'Only sub merchants can change their password from their profile.');
        }

        $payload = collect(request()->all())->except('_token')->all();

        $payload['premium_id']	= 170;
        $payload['email']		= request('email');

        $response = Ogaranya::post('auth/change-password', $payload);

        if ($response->status == 'Successful') {
            return redirect('/settings')->with('message', 'Password changed successfully.');
        }

        return redirect()->back()->with('error', (string)$response->data);
    }
}
