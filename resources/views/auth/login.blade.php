<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login - Probase</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="/img/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/vendors.css" />
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
</head>

<body class="bg-white">

    <div class="app">

        <div class="app-wrap">

            <div class="loader">
                <div class="h-100 d-flex justify-content-center">
                    <div class="align-self-center">
                        <img src="/img/loader/loader.svg" alt="loader">
                    </div>
                </div>
            </div>

            <div class="app-contant">
                <div class="bg-white">
                    <div class="container-fluid p-0">
                        <div class="row no-gutters">
                            <div class="col-sm-6 col-lg-5 col-xxl-3  align-self-center order-2 order-sm-1">
                                <div class="d-flex align-items-center h-100-vh">
                                    <div class="login p-50">
                                        <figure>
                                            <a href="#">
                                                <img src="/img/chatnbuy.JPG" alt="Brand Logo" width="200">
                                            </a>
                                        </figure>

                                        <p>Please login to your account.</p>

                                        <form method="POST" action="/login" class="mt-3 mt-sm-5">

                                            @csrf

                                            @include('includes.alert')

                                            <div class="row">
                                                <div class="col-12">

                                                    <div class="form-group">
                                                        <label class="form-label">Email*</label>
                                                        <input name="email" type="email" class="form-control" placeholder="Email Address" required />
                                                    </div>

                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Password*</label>
                                                        <input name="password" type="password" class="form-control" placeholder="Password" />
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="d-block d-sm-flex  align-items-center">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="gridCheck">
                                                            <label class="form-check-label" for="gridCheck">
                                                                Remember Me
                                                            </label>
                                                        </div>
                                                        <a href="reset_password.html" class="ml-auto">Forgot Password ?</a>
                                                    </div>
                                                </div>

                                                <div class="col-12 mt-3">
                                                    <button type="submit" class="btn btn-primary text-uppercase">Sign In</button>
                                                </div>


                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xxl-9 col-lg-7 bg-gradient o-hidden order-1 order-sm-2">
                                <div class="row align-items-center h-100">
                                    <div class="col-7 mx-auto ">
                                        <img class="img-fluid" src="/img/bg/login.svg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <script src="/js/vendors.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/jquery-1.11.2.min.js"></script>
</body>

</html>
