@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 m-b-30">
        <div class="d-block d-sm-flex flex-nowrap align-items-center">
            <div class="page-title mb-2 mb-sm-0">
                <h1>Order - {{$order->order_reference}}</h1>
            </div>
        </div>
    </div>

</div>

<div class="row tabs-contant">
    <div class="col-xxl-12  ">

        <div class="card p-20">
            <div class="card-header">
                <h4>Order Information</h4>
            </div>

            <div class="card-content row mt-5">
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="row">

                        <div class="form-group col-sm-6 col-md-6 col-lg-6">
                            <label for="referenceNumber">Transaction Reference Number</label>
                            <input type="text" value="{{$order->order_reference}}" disabled class="form-control  input-box b-white">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="paymentStatus">Payment Status</label><br>
                            <input type="text" value="{{$order->order_status}} | {{strtoupper($order->payment_type)}}" disabled class="form-control  input-box b-white">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="transactionTotal">Transaction Total</label>
                            <input type="text" id="transactionTotal" value="{{_c($order->total)}}" disabled class="form-control  input-box b-white">
                        </div>

                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="transactionDate">Transaction Date</label>
                            <input type="text" name="transactionDate" id="transactionDate" value="{{_d($order->date_ordered)}}" disabled class="form-control  input-box b-white">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="paymentDate">Payment Date</label>
                            <input type="text" name="paymentDate" id="paymentDate" value="{{_d($order->payment_date)}}" disabled class="form-control  input-box b-white">
                        </div>
                        <div class="form-group col-sm-6 col-lg-6">
                            <label for="customerPhoneNumber">Customer Phone Number</label>
                            <input type="text" name="customerPhoneNumber" id="customerPhoneNumber" value="{{$order->msisdn}}" disabled class="form-control input-box b-white">
                        </div>
                        <div class="form-group col-sm-12 col-lg-12">
                            <label for="shippingAddress"><i class="icon icon-shipping mr-2"></i> Shipping Address</label>
                            <input type="text" name="shippingAddress" id="shippingAddress" value="{{$order->shipping_address}}" disabled class="form-control input-box b-white">
                        </div>

                        <div class="col-lg-12 col-sm-12">

                            <h4>Order Details</h4>
                            
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>CODE</th>
                                        <th>PRICE EACH</th>
                                        <th>QUANTITY</th>
                                        <th>PRICE TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($order->order_details as $row)
                                    <tr>
                                        <td>{{$row->product->product_code}}</td>
                                        <td>{{$row->product->product_price}}</td>
                                        <td>{{$row->quantity}}</td>
                                        <td>{{(int)$row->product->product_price * (int)$row->quantity}}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

@endsection