@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 m-b-30">
        <div class="d-block d-sm-flex flex-nowrap align-items-center">
            <div class="page-title mb-2 mb-sm-0">
                <h1>Profile</h1>
            </div>
        </div>
    </div>

</div>

<div class="row tabs-contant">
    <div class="col-xxl-12  ">
        <div class="card card-statistics">
            
            <div class="card-body">
                    
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="row">

                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="firstName">Merchant Name</label>
                                <input type="text" value="{{optional($user->merchant)->merchant_name}}" name="firstName" id="firstName" disabled class="form-control  input-box b-white">
                            </div>
                            
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="phoneNumber">Phone Number</label>
                                <input type="text" value="{{optional($user->merchant)->merchant_phone}}" name="phoneNumber" id="phoneNumber" disabled class="form-control  input-box b-white">
                            </div>
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="emailAddress">Email Address</label>
                                <input type="text" value="{{optional($user->merchant)->merchant_email}}" name="emailAddress" id="emailAddress" disabled class="form-control  input-box b-white">
                            </div>

                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="nameOfBusiness">Name of Business</label>
                                <input type="text" value="{{optional($user->merchant)->merchant_name}}" name="nameOfBusiness" id="nameOfBusiness" disabled class="form-control  input-box b-white">
                            </div>
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="state">State</label>
                                <input type="text" name="state" id="state" value="Lagos" disabled class="form-control  input-box b-white">
                            </div>
                            
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="country">Country</label>
                                <input type="text" name="country" id="country" value="Nigeria" disabled class="form-control  input-box b-white">
                            </div>                  
                            
                        </div>

                        <form method="POST" action="/admin/settings/password" class="row">

                            @csrf

                            <p class="col-sm-12 col-lg-12 mt-2">Password settings</p>
                            <hr>
                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="country">New Password</label>
                                <input type="password" name="password" required class="form-control  input-box b-white">
                            </div>

                            <div class="form-group col-sm-6 col-lg-6">
                                <label for="country">Retype New Password</label>
                                <input type="password" name="password_confirmation" required class="form-control  input-box b-white">
                            </div>
                            <div class="form-group col-12">
                                <input type="hidden" name="email" value="{{optional($user->merchant)->merchant_email}}">
                                <button type="submit" class="btn btn-primary">Change Password</button>
                            </div>
                        </form>


                    </div>

                </div>

        </div>
    </div>
</div>

@endsection