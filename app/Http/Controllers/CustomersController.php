<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Ogaranya;

class CustomersController extends Controller
{
	public function index()
	{
		$data['active'] 	= 'customers';
		$data['customers'] 	= Ogaranya::get('customers?q='.request('query'))->data;
		
		return view('customers.index', $data);
	}

	public function subscribers()
	{
		$data['active'] 		= 'subscribers';
		$data['subscribers'] 	= Ogaranya::get('subscribers?q='.request('query'))->data;
		
		return view('customers.subscribers', $data);
	}
}
