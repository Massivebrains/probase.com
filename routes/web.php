<?php

use Illuminate\Support\Facades\Route;

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');
Route::any('/forgot-password', 'AuthController@forgotPassword');
Route::any('/reset-password/{token?}', 'AuthController@resetPassword');

Route::group(['middleware' => 'custom-auth'], function(){

	Route::get('/', 'HomeController@index');
	
	Route::get('/products', 'ProductsController@index');
	Route::get('/product/{id}/{type?}', 'ProductsController@single');
	Route::post('/product/{id}', 'ProductsController@save');

	Route::get('/orders', 'OrdersController@index');
	Route::get('/order/{id}', 'OrdersController@single');

	Route::get('/wallet', 'WalletController@index');
	Route::get('/customers', 'CustomersController@index');
	Route::get('/subscribers', 'CustomersController@subscribers');
	Route::get('/account', 'AccountController@index');
	Route::post('/account', 'AccountController@save');
	Route::get('/settings', 'AccountController@settings');
	Route::post('/settings/password', 'AccountController@password');
	Route::get('/merchants', 'MerchantsController@index');
	Route::get('/merchant/{id}', 'MerchantsController@single');
	Route::post('/merchant/{id}', 'MerchantsController@save');
	
});

Route::any('/bots/slack', 'BotsController@slack');