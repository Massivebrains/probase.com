@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-8 m-b-30">
            <div class="d-block d-sm-flex flex-nowrap align-items-center">
                <div class="page-title mb-2 mb-sm-0">
                    <h1>Merchants</h1>
                </div>
            </div>
        </div>

    </div>

    <div class="row tabs-contant">
        <div class="col-xxl-12  ">
            <div class="card card-statistics">
                <div class="card-body">

                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email Address</th>
                                <th>Phone</th>
                                <th>Wallet Balance</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($merchants as $row)
                                <tr>
                                    <td>{{ $row->merchant_name }}</td>
                                    <td>{{ $row->merchant_email }}</td>
                                    <td>{{ $row->merchant_phone }}</td>
                                    <td>{{ $row->wallet }}</td>
                                    <td><a href="/merchant/{{ $row->id }}">Edit</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection
