<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Ogaranya;

class WalletController extends Controller
{
    public function index()
    {
    	$data['active']     	  = 'wallet';
        $data['transactions']     = Ogaranya::get('transactions?q='.request('query'))->data;

    	return view('wallet.index', $data);
    }
}
